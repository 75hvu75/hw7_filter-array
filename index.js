'use strict';

let myArr = ['hello', 'world', 23, '23', null];
let myType = 'string';

function filterBy(arr, type) {
    let filteredArr = arr.filter(element => typeof element !== type);
    return filteredArr;
}

let result = filterBy(myArr, myType);

console.log(result);
